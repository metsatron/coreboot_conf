#include <device/device.h>
#include <device/pci.h>
#include "cpu/intel/model_206ax/chip.h"
#include "northbridge/intel/sandybridge/chip.h"
#include "southbridge/intel/bd82x6x/chip.h"
#include "superio/ite/it8728f/chip.h"

#if !DEVTREE_EARLY
__attribute__((weak)) struct chip_operations mainboard_ops = {};
__attribute__((weak)) struct chip_operations cpu_intel_model_206ax_ops = {};
__attribute__((weak)) struct chip_operations cpu_intel_socket_LGA1155_ops = {};
__attribute__((weak)) struct chip_operations northbridge_intel_sandybridge_ops = {};
__attribute__((weak)) struct chip_operations southbridge_intel_bd82x6x_ops = {};
__attribute__((weak)) struct chip_operations superio_ite_it8728f_ops = {};
#endif

/* pass 0 */
DEVTREE_CONST struct bus dev_root_links[];
DEVTREE_CONST static struct device _dev2;
DEVTREE_CONST struct bus _dev2_links[];
DEVTREE_CONST static struct device _dev7;
DEVTREE_CONST struct bus _dev7_links[];
DEVTREE_CONST static struct device _dev4;
DEVTREE_CONST static struct device _dev6;
DEVTREE_CONST static struct device _dev8;
DEVTREE_CONST static struct device _dev9;
DEVTREE_CONST static struct device _dev10;
DEVTREE_CONST static struct device _dev12;
DEVTREE_CONST static struct device _dev13;
DEVTREE_CONST static struct device _dev14;
DEVTREE_CONST static struct device _dev15;
DEVTREE_CONST static struct device _dev16;
DEVTREE_CONST static struct device _dev17;
DEVTREE_CONST static struct device _dev18;
DEVTREE_CONST static struct device _dev19;
DEVTREE_CONST static struct device _dev20;
DEVTREE_CONST static struct device _dev21;
DEVTREE_CONST static struct device _dev22;
DEVTREE_CONST static struct device _dev23;
DEVTREE_CONST static struct device _dev24;
DEVTREE_CONST struct bus _dev24_links[];
DEVTREE_CONST static struct device _dev26;
DEVTREE_CONST static struct device _dev27;
DEVTREE_CONST static struct device _dev28;
DEVTREE_CONST static struct device _dev29;
DEVTREE_CONST static struct device _dev30;
DEVTREE_CONST static struct device _dev31;
DEVTREE_CONST struct bus _dev31_links[];
DEVTREE_CONST static struct device _dev42;
DEVTREE_CONST static struct device _dev43;
DEVTREE_CONST static struct device _dev44;
DEVTREE_CONST static struct device _dev45;
DEVTREE_CONST static struct device _dev25;
DEVTREE_CONST static struct device _dev33;
DEVTREE_CONST static struct device _dev34;
DEVTREE_CONST struct resource _dev34_res[];
DEVTREE_CONST static struct device _dev35;
DEVTREE_CONST struct resource _dev35_res[];
DEVTREE_CONST static struct device _dev36;
DEVTREE_CONST struct resource _dev36_res[];
DEVTREE_CONST static struct device _dev37;
DEVTREE_CONST struct resource _dev37_res[];
DEVTREE_CONST static struct device _dev38;
DEVTREE_CONST struct resource _dev38_res[];
DEVTREE_CONST static struct device _dev39;
DEVTREE_CONST struct resource _dev39_res[];
DEVTREE_CONST static struct device _dev40;
DEVTREE_CONST static struct device _dev41;

/* pass 1 */
DEVTREE_CONST struct device * DEVTREE_CONST last_dev = &_dev45;
DEVTREE_CONST struct device dev_root = {
#if !DEVTREE_EARLY
	.ops = &default_dev_ops_root,
#endif
	.bus = &dev_root_links[0],
	.path = { .type = DEVICE_PATH_ROOT },
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = &dev_root_links[0],
#if !DEVTREE_EARLY
	.chip_ops = &mainboard_ops,
	.name = mainboard_name,
#endif
	.next=&_dev2
};
DEVTREE_CONST struct bus dev_root_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &dev_root,
			.children = &_dev2,
			.next = NULL,
		},
	};
DEVTREE_CONST struct northbridge_intel_sandybridge_config northbridge_intel_sandybridge_info_1 = {
	.gfx.did = { 0x80000100, 0x80000240, 0x80000410, 0x80000410, 0x00000005 },
	.gfx.ndid = 3,
	.pci_mmio_size = 2048,
};

static DEVTREE_CONST struct device _dev2 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &dev_root_links[0],
	.path = {.type=DEVICE_PATH_CPU_CLUSTER,{.cpu_cluster={ .cluster = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = &_dev2_links[0],
	.sibling = &_dev7,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev4
};
DEVTREE_CONST struct bus _dev2_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev2,
			.children = &_dev4,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev7 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &dev_root_links[0],
	.path = {.type=DEVICE_PATH_DOMAIN,{.domain={ .domain = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = &_dev7_links[0],
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev8
};
DEVTREE_CONST struct bus _dev7_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev7,
			.children = &_dev8,
			.next = NULL,
		},
	};
DEVTREE_CONST struct cpu_intel_model_206ax_config cpu_intel_model_206ax_info_5 = {
	.c1_acpower = 1,
	.c1_battery = 1,
	.c2_acpower = 3,
	.c2_battery = 3,
	.c3_acpower = 5,
	.c3_battery = 5,
};

static DEVTREE_CONST struct device _dev4 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev2_links[0],
	.path = {.type=DEVICE_PATH_APIC,{.apic={ .apic_id = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev6,
#if !DEVTREE_EARLY
	.chip_ops = &cpu_intel_socket_LGA1155_ops,
#endif
	.next=&_dev6
};
static DEVTREE_CONST struct device _dev6 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev2_links[0],
	.path = {.type=DEVICE_PATH_APIC,{.apic={ .apic_id = 0xacac }}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &cpu_intel_model_206ax_ops,
#endif
	.chip_info = &cpu_intel_model_206ax_info_5,
	.next=&_dev7
};
static DEVTREE_CONST struct device _dev8 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x0,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev9,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev9
};
static DEVTREE_CONST struct device _dev9 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev10,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev10
};
static DEVTREE_CONST struct device _dev10 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x2,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0xd000,
	.link_list = NULL,
	.sibling = &_dev12,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev12
};
DEVTREE_CONST struct southbridge_intel_bd82x6x_config southbridge_intel_bd82x6x_info_11 = {
	.alt_gp_smi_en = 0x0000,
	.c2_latency = 0x0065,
	.docking_supported = 0,
	.gen1_dec = 0x003c0a01,
	.p_cnt_throttling_supported = 0,
	.pcie_port_coalesce = 0,
	.sata_interface_speed_support = 0x3,
	.sata_port_map = 0x3f,
};

static DEVTREE_CONST struct device _dev12 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x14,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5007,
	.link_list = NULL,
	.sibling = &_dev13,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev13
};
static DEVTREE_CONST struct device _dev13 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev14,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev14
};
static DEVTREE_CONST struct device _dev14 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,1)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev15,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev15
};
static DEVTREE_CONST struct device _dev15 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,2)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev16,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev16
};
static DEVTREE_CONST struct device _dev16 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,3)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev17,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev17
};
static DEVTREE_CONST struct device _dev17 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x19,0)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev18,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev18
};
static DEVTREE_CONST struct device _dev18 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1a,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5006,
	.link_list = NULL,
	.sibling = &_dev19,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev19
};
static DEVTREE_CONST struct device _dev19 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1b,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0xa002,
	.link_list = NULL,
	.sibling = &_dev20,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev20
};
static DEVTREE_CONST struct device _dev20 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev21,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev21
};
static DEVTREE_CONST struct device _dev21 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,1)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev22,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev22
};
static DEVTREE_CONST struct device _dev22 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,2)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev23,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev23
};
static DEVTREE_CONST struct device _dev23 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,3)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev24,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev24
};
static DEVTREE_CONST struct device _dev24 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,4)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = &_dev24_links[0],
	.sibling = &_dev26,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev25
};
DEVTREE_CONST struct bus _dev24_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev24,
			.children = &_dev25,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev26 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,5)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev27,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev27
};
static DEVTREE_CONST struct device _dev27 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,6)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev28,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev28
};
static DEVTREE_CONST struct device _dev28 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,7)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev29,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev29
};
static DEVTREE_CONST struct device _dev29 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1d,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5006,
	.link_list = NULL,
	.sibling = &_dev30,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev30
};
static DEVTREE_CONST struct device _dev30 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1e,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev31,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev31
};
static DEVTREE_CONST struct device _dev31 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5001,
	.link_list = &_dev31_links[0],
	.sibling = &_dev42,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev33
};
DEVTREE_CONST struct bus _dev31_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev31,
			.children = &_dev33,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev42 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,2)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0xb005,
	.link_list = NULL,
	.sibling = &_dev43,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev43
};
static DEVTREE_CONST struct device _dev43 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,3)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5001,
	.link_list = NULL,
	.sibling = &_dev44,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev44
};
static DEVTREE_CONST struct device _dev44 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,4)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev45,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev45
};
static DEVTREE_CONST struct device _dev45 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,5)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
};
static DEVTREE_CONST struct device _dev25 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev24_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x0,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0xe000,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev26
};
DEVTREE_CONST struct superio_ite_it8728f_config superio_ite_it8728f_info_32 = { };
static DEVTREE_CONST struct device _dev33 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x0 }}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev34,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev34
};
static DEVTREE_CONST struct device _dev34 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x1 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev34_res[0],
	.link_list = NULL,
	.sibling = &_dev35,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev35
};
DEVTREE_CONST struct resource _dev34_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0x3f8,.next=&_dev34_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0x4,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev35 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x2 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev35_res[0],
	.link_list = NULL,
	.sibling = &_dev36,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev36
};
DEVTREE_CONST struct resource _dev35_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0x2f8,.next=&_dev35_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0x3,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev36 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x3 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev36_res[0],
	.link_list = NULL,
	.sibling = &_dev37,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev37
};
DEVTREE_CONST struct resource _dev36_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0x378,.next=&_dev36_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0x7,.next=&_dev36_res[2]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_DRQ, .index=0x74, .base=0x4,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev37 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x4 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev37_res[0],
	.link_list = NULL,
	.sibling = &_dev38,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev38
};
DEVTREE_CONST struct resource _dev37_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0xa30,.next=&_dev37_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0x9,.next=&_dev37_res[2]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x62, .base=0xa20,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev38 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x5 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev38_res[0],
	.link_list = NULL,
	.sibling = &_dev39,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev39
};
DEVTREE_CONST struct resource _dev38_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0x60,.next=&_dev38_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0x1,.next=&_dev38_res[2]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x62, .base=0x64,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev39 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x6 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.resource_list = &_dev39_res[0],
	.link_list = NULL,
	.sibling = &_dev40,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev40
};
DEVTREE_CONST struct resource _dev39_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IRQ, .index=0x70, .base=0xc,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev40 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0x7 }}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
	.sibling = &_dev41,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev41
};
static DEVTREE_CONST struct device _dev41 = {
#if !DEVTREE_EARLY
	.ops = 0,
#endif
	.bus = &_dev31_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0x2e, .device = 0xa }}},
	.enabled = 0,
	.on_mainboard = 1,
	.subsystem_vendor = 0x1458,
	.subsystem_device = 0x5000,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &superio_ite_it8728f_ops,
#endif
	.chip_info = &superio_ite_it8728f_info_32,
	.next=&_dev42
};
