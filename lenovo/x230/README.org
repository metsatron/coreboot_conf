* Board:lenovo/x230

** Status

[[https://www.coreboot.org/Intel_Native_Raminit][Intel_Native_Raminit]] has it's own status page.

Thanks for your interest in Lenovo X230 port.

Issues:

    Badly seated RAM may prevent booting (not really a problem but coreboot is more suspicious to this than vendor BIOS)
    Automatic screen rotation will not work on Windows 10 (X230T)
    Buttons for screen rotation (X230T) and microphone mute don't work on Windows 10 (the first issue also causes the "Tablet Service" tsmservice.exe to hog one cpu core. It needs to be disabled.)
    eGPU will not work on Windows 10 (ACPI BIOS ERROR bluescreen) - tested with a PE4C and NVidia GT730 attached via ExpressCard
    UltraNav driver for the TrackPoint needs to be force-installed via device manager on Windows 10. It will only detect it as a PS/2 mouse otherwise.

Tested:

  * S3 (Suspend to RAM)
  * RAM module combinations of 8G+8G, 8G+0, 0+8G, 4G+8G, 8G+4G, 8G+1G, 1G+0, 0+1G, 4G+0, 0+4G
  * USB (both 2.0 and 3.0 ports)
  * Video (both internal and VGA)
  * eGPU (works on Linux with 16GB of RAM - not on Windows, see above)
  * Expresscard slot (including hotplugging)
  * Sound (integrated speakers, integrated mic, external headphones, external mic)
  * LAN
  * mini-PCIe slots (both wlan and wwan)
  * Linux (through GRUB-as-payload)
  * Windows (through GRUB-as-payload loading SeaBIOS image from disk; you have to use extracted VGA blob, dumped from memory isn't good enough)
  * SD card slot
  * Thermal management
  * Fingerprint reader.
  * Webcam
  * trackpoint
  * touchpad
  * Fn hotkeys
  * Keyboard backlight
  * Thinklight.
  * bluetooth
  * dock (UltraBase Series 3 for X220 works, too)
  * msata (fixed in commit c8f54a1109072706e2fa091dc9ab4ad3eb057b42)
  * mini DisplayPort + fullsize DisplayPort (X230T)
  * digitizer + touchscreen on x230t variant

** Proprietary components' status

  * CPU Microcode
  * VGA Option ROM (optional): you need it if you want graphics in several bootloaders, and proprietary OS
  * ME (Management Engine) => you do not have to touch it (just leave it where it is)
  * EC (Embedded Controller) => you do not have to touch it (just leave it where it is)

** Code

  * The code has been merged into coreboot master:

=$ git clone https://review.coreboot.org/coreboot.git=


** Building Firmware

Please have a look at [[https://www.coreboot.org/Intel_Sandybridge_Build_Tutorial][Intel_Sandybridge_Build_Tutorial]].

It's actually sufficient to just flash the 4M chip (the one positioned at the top, marked spi1) with an image with ROM size of 12M, CBFS of 4M, and a fake IFD, for example with the SeaBIOS payload, or a small Linux payload with Busybox and kexec installed to load kernels. This is because the 4M chip holds the end of the concatenated 12M "opaque flash chip"'s 7M BIOS region, and is largely hardware specific (do *not* flash a Coreboot ROM with a fake IFD to a whole system on other systems!). This is sufficient to boot the system, and will allow one to flash the BIOS region internally later now that Coreboot is installed, taking advantage of the whole 7M (with default layout and not touching the other regions) BIOS region.
** Flashing
*Hardware Flashing*

*NOTICE:* There has been at least one report of a bricked laptop due to just reading the flash chips' content with an external programmer. Apparently something on the board may break by applying external power.

X230 has 2 flash chips of 8M and 4M. They're concatenated to one virtual flash chip of 12M (the 8M chip is the first portion of this virtual chip, and the 4M is the final portion) which is itself subdivided in roughly in 3 parts:

    Descriptor (12K)
    ME firmware (5M-12K)
    System flash (7M)

ME firmware is not readable. Vendor firmware locks the flash and so you need to flash externally.

Proceeds as follows:

  * Turn off your laptop, remove battery and AC adapter.
  * Remove the keyboard.
  * Connect your external SPI flasher to the top SPI chip which is under palm resting space, on left side of the board. It's a 4M chip. IF you've chosen CBFS_SIZE 4M or smaller that' the only chip you need to reflash.

Use of an SOIC-8 clip such as Pomona 5250 is recommended. You should ideally power flash using the wake-on-LAN (WoL) feature. If that doesn't work, be extremely careful not to supply more than 3.3V to flash (note that this is dangerous in this case). WoL provides the correct voltage and current in a stable manner, and does not power other parts of the board. Be careful not to connect VCC from your programmer or an external power supply while powering flash using the WoL feature.

  * Read the flash. Twice. Compare the files to be sure. Save a copy of it on

external media.

#+BEGIN_SRC sh
flashrom -p <yourprogrammer> -r flash.bin
flashrom -p <yourprogrammer> -r flash2.bin
diff flash.bin flash2.bin
#+END_SRC

If they don't match, do not proceed. If the file is 8M, you're flashing wrong chip, connect to the right one.

  * Write the flash. Since you have to write only top 4M, first split out those 4M:

 =dd of=top.rom bs=1M if=build/coreboot.rom skip=8=

  * Use flashrom to flash top.rom.

If you have trouble reading the chip successfully, the most common problems are

  * insufficient power supply
  * bad contacts
  * too long wires
  * bad pinout

See also [[http://flashrom.org/ISP][In-System Programming]]
*Internal Flashing*

You can flash internally with flashrom after IFD is unlocked and you have coreboot installed.

To unlock IFD, use ``ifdtool`` to unlock the 8M part and reflash it:

=ifdtool -u ifdmegbe.rom=

The flashrom output is as follows:
#+BEGIN_SRC sh
flashrom v0.9.9-rc1-r1942 on Linux 4.4.0-21-generic (x86_64)
flashrom is free software, get the source code at https://flashrom.org

Calibrating delay loop... OK.
coreboot table found at 0xbff4d000.
Found chipset "Intel QM77".
Enabling flash write... Enabling hardware sequencing due to multiple flash chips detected.
OK.
Found Programmer flash chip "Opaque flash chip" (12288 kB, Programmer-specific) mapped at physical address 0x0000000000000000.
#+END_SRC

You can flash both two chips. You can also flash just the top 4M with a layout file.

x230-layout.txt:
#+BEGIN_SRC sh
0x00000000:0x007fffff ifdmegbe
0x00800000:0x00bfffff bios
#+END_SRC

flashrom command:

=flashrom -p internal --layout x230-layout.txt --image bios --write build/coreboot.rom=

*Error handling*

*NOTICE:* If it takes several minutes booting the laptop for the first time after flashing a new image, during which the screen remains black, then you have done something wrong. You likely corrupted the ME region or used an unnatural alignment of flash regions. When flashing coreboot you should never touch a flash region besides the one labeled "bios".
Setting battery threshold on a X230 with coreboot

The [[https://github.com/teleshoes/tpacpi-bat][tpacpi-bat]] utility doesn't work after flashing coreboot. An alternative is using the coreboot util ectool. One can set the lower and upper charging threshold by writing the desired battery charge percentage (in hex) to addresses 0xb0 and 0xb1 respectively. So to start charging only when the battery is below 40% (0x28 in hex), one would execute:

=# ectool -w 0xb0 -z 0x28=

To stop charging an 70% (0x46 in hex):

=# ectool -w 0xb1 -z 0x46=

Hex values can be determined e.g. using *printf*:
#+BEGIN_SRC sh
$ printf '%x\n' 70
46
#+END_SRC

This can then be run as a script on startup to ensure permanence. 
