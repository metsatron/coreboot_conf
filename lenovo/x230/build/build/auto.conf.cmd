deps_config := \
	src/lib/Kconfig \
	payloads/external/Memtest86Plus/Kconfig.secondary \
	payloads/external/FILO/Kconfig \
	payloads/external/GRUB2/Kconfig \
	payloads/external/LinuxBoot/Kconfig \
	payloads/external/SeaBIOS/Kconfig \
	payloads/external/U-Boot/Kconfig \
	payloads/external/depthcharge/Kconfig \
	payloads/external/iPXE/Kconfig \
	payloads/external/linux/Kconfig \
	payloads/external/tianocore/Kconfig \
	payloads/external/FILO/Kconfig.name \
	payloads/external/GRUB2/Kconfig.name \
	payloads/external/LinuxBoot/Kconfig.name \
	payloads/external/SeaBIOS/Kconfig.name \
	payloads/external/U-Boot/Kconfig.name \
	payloads/external/depthcharge/Kconfig.name \
	payloads/external/linux/Kconfig.name \
	payloads/external/tianocore/Kconfig.name \
	payloads/Kconfig \
	src/console/Kconfig \
	src/acpi/Kconfig \
	src/security/tpm/tss/vendor/cr50/Kconfig \
	src/security/tpm/Kconfig \
	src/security/vboot/Kconfig \
	src/security/Kconfig \
	src/commonlib/storage/Kconfig \
	src/drivers/amd/agesa/Kconfig \
	src/drivers/aspeed/ast2050/Kconfig \
	src/drivers/aspeed/common/Kconfig \
	src/drivers/dec/21143/Kconfig \
	src/drivers/emulation/qemu/Kconfig \
	src/drivers/generic/adau7002/Kconfig \
	src/drivers/generic/bayhub/Kconfig \
	src/drivers/generic/generic/Kconfig \
	src/drivers/generic/gpio_keys/Kconfig \
	src/drivers/generic/gpio_regulator/Kconfig \
	src/drivers/generic/ioapic/Kconfig \
	src/drivers/generic/max98357a/Kconfig \
	src/drivers/i2c/adm1026/Kconfig \
	src/drivers/i2c/adm1027/Kconfig \
	src/drivers/i2c/adt7463/Kconfig \
	src/drivers/i2c/at24rf08c/Kconfig \
	src/drivers/i2c/ck505/Kconfig \
	src/drivers/i2c/da7219/Kconfig \
	src/drivers/i2c/designware/Kconfig \
	src/drivers/i2c/generic/Kconfig \
	src/drivers/i2c/hid/Kconfig \
	src/drivers/i2c/i2cmux/Kconfig \
	src/drivers/i2c/i2cmux2/Kconfig \
	src/drivers/i2c/lm63/Kconfig \
	src/drivers/i2c/max98373/Kconfig \
	src/drivers/i2c/max98927/Kconfig \
	src/drivers/i2c/nau8825/Kconfig \
	src/drivers/i2c/pca9538/Kconfig \
	src/drivers/i2c/pcf8523/Kconfig \
	src/drivers/i2c/rt5663/Kconfig \
	src/drivers/i2c/rtd2132/Kconfig \
	src/drivers/i2c/rx6110sa/Kconfig \
	src/drivers/i2c/sx9310/Kconfig \
	src/drivers/i2c/tpm/Kconfig \
	src/drivers/i2c/w83793/Kconfig \
	src/drivers/i2c/w83795/Kconfig \
	src/drivers/i2c/ww_ring/Kconfig \
	src/drivers/intel/fsp1_1/Kconfig \
	src/drivers/intel/fsp2_0/Kconfig \
	src/drivers/intel/gma/Kconfig \
	src/drivers/intel/i210/Kconfig \
	src/drivers/intel/mipi_camera/Kconfig \
	src/drivers/intel/wifi/Kconfig \
	src/drivers/lenovo/hybrid_graphics/Kconfig \
	src/drivers/maxim/max77686/Kconfig \
	src/drivers/parade/ps8625/Kconfig \
	src/drivers/parade/ps8640/Kconfig \
	src/drivers/pc80/pc/Kconfig \
	src/drivers/pc80/rtc/Kconfig \
	src/drivers/pc80/tpm/Kconfig \
	src/drivers/pc80/vga/Kconfig \
	src/drivers/ricoh/rce822/Kconfig \
	src/drivers/siemens/nc_fpga/Kconfig \
	src/drivers/sil/3114/Kconfig \
	src/drivers/spi/acpi/Kconfig \
	src/drivers/spi/tpm/Kconfig \
	src/drivers/ti/tps65090/Kconfig \
	src/drivers/ti/tps65913/Kconfig \
	src/drivers/usb/acpi/Kconfig \
	src/drivers/xgi/common/Kconfig \
	src/drivers/xgi/z9s/Kconfig \
	src/drivers/xpowers/axp209/Kconfig \
	src/drivers/ams/Kconfig \
	src/drivers/asmedia/Kconfig \
	src/drivers/elog/Kconfig \
	src/drivers/gic/Kconfig \
	src/drivers/ipmi/Kconfig \
	src/drivers/lenovo/Kconfig \
	src/drivers/mrc_cache/Kconfig \
	src/drivers/net/Kconfig \
	src/drivers/spi/Kconfig \
	src/drivers/tpm/Kconfig \
	src/drivers/uart/Kconfig \
	src/drivers/usb/Kconfig \
	src/device/Kconfig \
	src/arch/arm64/armv8/Kconfig \
	src/arch/arm/armv7/Kconfig \
	src/arch/arm/armv4/Kconfig \
	src/arch/arm/Kconfig \
	src/arch/arm64/Kconfig \
	src/arch/mips/Kconfig \
	src/arch/power8/Kconfig \
	src/arch/riscv/Kconfig \
	src/arch/x86/Kconfig \
	src/vendorcode/google/chromeos/Kconfig \
	src/vendorcode/amd/pi/Kconfig \
	src/vendorcode/amd/Kconfig \
	src/vendorcode/cavium/Kconfig \
	src/vendorcode/google/Kconfig \
	src/vendorcode/intel/Kconfig \
	src/vendorcode/siemens/Kconfig \
	src/southbridge/intel/common/firmware/Kconfig \
	src/drivers/intel/fsp1_0/Kconfig \
	src/ec/compal/ene932/Kconfig \
	src/ec/google/chromeec/Kconfig \
	src/ec/hp/kbc1126/Kconfig \
	src/ec/kontron/it8516e/Kconfig \
	src/ec/lenovo/h8/Kconfig \
	src/ec/lenovo/pmh7/Kconfig \
	src/ec/purism/librem/Kconfig \
	src/ec/quanta/ene_kb3940q/Kconfig \
	src/ec/quanta/it8518/Kconfig \
	src/ec/roda/it8518/Kconfig \
	src/ec/smsc/mec1308/Kconfig \
	src/ec/acpi/Kconfig \
	src/superio/fintek/common/Kconfig \
	src/superio/fintek/f71805f/Kconfig \
	src/superio/fintek/f71808a/Kconfig \
	src/superio/fintek/f71859/Kconfig \
	src/superio/fintek/f71863fg/Kconfig \
	src/superio/fintek/f71869ad/Kconfig \
	src/superio/fintek/f71872/Kconfig \
	src/superio/fintek/f81216h/Kconfig \
	src/superio/fintek/f81865f/Kconfig \
	src/superio/fintek/f81866d/Kconfig \
	src/superio/intel/i8900/Kconfig \
	src/superio/ite/common/Kconfig \
	src/superio/ite/it8623e/Kconfig \
	src/superio/ite/it8671f/Kconfig \
	src/superio/ite/it8712f/Kconfig \
	src/superio/ite/it8716f/Kconfig \
	src/superio/ite/it8718f/Kconfig \
	src/superio/ite/it8720f/Kconfig \
	src/superio/ite/it8721f/Kconfig \
	src/superio/ite/it8728f/Kconfig \
	src/superio/ite/it8772f/Kconfig \
	src/superio/ite/it8783ef/Kconfig \
	src/superio/nsc/pc87309/Kconfig \
	src/superio/nsc/pc87360/Kconfig \
	src/superio/nsc/pc87366/Kconfig \
	src/superio/nsc/pc87382/Kconfig \
	src/superio/nsc/pc87384/Kconfig \
	src/superio/nsc/pc87392/Kconfig \
	src/superio/nsc/pc87417/Kconfig \
	src/superio/nsc/pc97317/Kconfig \
	src/superio/nuvoton/common/Kconfig \
	src/superio/nuvoton/nct5104d/Kconfig \
	src/superio/nuvoton/nct5572d/Kconfig \
	src/superio/nuvoton/nct6776/Kconfig \
	src/superio/nuvoton/nct6779d/Kconfig \
	src/superio/nuvoton/nct6791d/Kconfig \
	src/superio/nuvoton/npcd378/Kconfig \
	src/superio/nuvoton/wpcm450/Kconfig \
	src/superio/renesas/m3885x/Kconfig \
	src/superio/serverengines/pilot/Kconfig \
	src/superio/smsc/dme1737/Kconfig \
	src/superio/smsc/fdc37n972/Kconfig \
	src/superio/smsc/kbc1100/Kconfig \
	src/superio/smsc/lpc47b272/Kconfig \
	src/superio/smsc/lpc47b397/Kconfig \
	src/superio/smsc/lpc47m10x/Kconfig \
	src/superio/smsc/lpc47m15x/Kconfig \
	src/superio/smsc/lpc47n207/Kconfig \
	src/superio/smsc/lpc47n217/Kconfig \
	src/superio/smsc/lpc47n227/Kconfig \
	src/superio/smsc/mec1308/Kconfig \
	src/superio/smsc/sch4037/Kconfig \
	src/superio/smsc/sio1007/Kconfig \
	src/superio/smsc/sio1036/Kconfig \
	src/superio/smsc/sio10n268/Kconfig \
	src/superio/smsc/smscsuperio/Kconfig \
	src/superio/via/vt1211/Kconfig \
	src/superio/winbond/common/Kconfig \
	src/superio/winbond/w83627dhg/Kconfig \
	src/superio/winbond/w83627ehg/Kconfig \
	src/superio/winbond/w83627hf/Kconfig \
	src/superio/winbond/w83627thg/Kconfig \
	src/superio/winbond/w83627uhg/Kconfig \
	src/superio/winbond/w83667hg-a/Kconfig \
	src/superio/winbond/w83697hf/Kconfig \
	src/superio/winbond/w83977tf/Kconfig \
	src/superio/winbond/wpcd376i/Kconfig \
	src/southbridge/amd/pi/hudson/Kconfig \
	src/southbridge/amd/cimx/sb900/Kconfig \
	src/southbridge/amd/cimx/sb800/Kconfig \
	src/southbridge/amd/agesa/hudson/Kconfig \
	src/southbridge/amd/agesa/Kconfig \
	src/southbridge/amd/amd8111/Kconfig \
	src/southbridge/amd/amd8132/Kconfig \
	src/southbridge/amd/cimx/Kconfig \
	src/southbridge/amd/cs5536/Kconfig \
	src/southbridge/amd/pi/Kconfig \
	src/southbridge/amd/rs780/Kconfig \
	src/southbridge/amd/sb700/Kconfig \
	src/southbridge/amd/sb800/Kconfig \
	src/southbridge/amd/sr5650/Kconfig \
	src/southbridge/broadcom/bcm21000/Kconfig \
	src/southbridge/broadcom/bcm5785/Kconfig \
	src/southbridge/intel/bd82x6x/Kconfig \
	src/southbridge/intel/common/Kconfig \
	src/southbridge/intel/fsp_bd82x6x/Kconfig \
	src/southbridge/intel/fsp_i89xx/Kconfig \
	src/southbridge/intel/fsp_rangeley/Kconfig \
	src/southbridge/intel/i82371eb/Kconfig \
	src/southbridge/intel/i82801dx/Kconfig \
	src/southbridge/intel/i82801gx/Kconfig \
	src/southbridge/intel/i82801ix/Kconfig \
	src/southbridge/intel/i82801jx/Kconfig \
	src/southbridge/intel/i82870/Kconfig \
	src/southbridge/intel/ibexpeak/Kconfig \
	src/southbridge/intel/lynxpoint/Kconfig \
	src/southbridge/nvidia/ck804/Kconfig \
	src/southbridge/nvidia/mcp55/Kconfig \
	src/southbridge/ricoh/rl5c476/Kconfig \
	src/southbridge/ti/pci1x2x/Kconfig \
	src/southbridge/ti/pci7420/Kconfig \
	src/southbridge/ti/pcixx12/Kconfig \
	src/northbridge/intel/fsp_sandybridge/fsp/Kconfig \
	src/northbridge/intel/fsp_rangeley/fsp/Kconfig \
	src/northbridge/amd/pi/00660F01/Kconfig \
	src/northbridge/amd/pi/00730F01/Kconfig \
	src/northbridge/amd/pi/00630F01/Kconfig \
	src/northbridge/amd/agesa/family12/Kconfig \
	src/northbridge/amd/agesa/family14/Kconfig \
	src/northbridge/amd/agesa/family15tn/Kconfig \
	src/northbridge/amd/agesa/family16kb/Kconfig \
	src/northbridge/amd/agesa/Kconfig \
	src/northbridge/amd/amdfam10/Kconfig \
	src/northbridge/amd/lx/Kconfig \
	src/northbridge/amd/pi/Kconfig \
	src/northbridge/intel/e7505/Kconfig \
	src/northbridge/intel/fsp_rangeley/Kconfig \
	src/northbridge/intel/fsp_sandybridge/Kconfig \
	src/northbridge/intel/gm45/Kconfig \
	src/northbridge/intel/haswell/Kconfig \
	src/northbridge/intel/i440bx/Kconfig \
	src/northbridge/intel/i945/Kconfig \
	src/northbridge/intel/nehalem/Kconfig \
	src/northbridge/intel/pineview/Kconfig \
	src/northbridge/intel/sandybridge/Kconfig \
	src/northbridge/intel/x4x/Kconfig \
	src/northbridge/via/vx900/Kconfig \
	src/cpu/via/nano/Kconfig \
	src/cpu/ti/am335x/Kconfig \
	src/cpu/intel/common/Kconfig \
	src/cpu/intel/turbo/Kconfig \
	src/cpu/intel/fit/Kconfig \
	src/cpu/intel/socket_rPGA989/Kconfig \
	src/cpu/intel/socket_rPGA988B/Kconfig \
	src/cpu/intel/socket_LGA775/Kconfig \
	src/cpu/intel/socket_LGA1155/Kconfig \
	src/cpu/intel/socket_441/Kconfig \
	src/cpu/intel/socket_mPGA604/Kconfig \
	src/cpu/intel/socket_mPGA478MN/Kconfig \
	src/cpu/intel/socket_mFCPGA478/Kconfig \
	src/cpu/intel/socket_FCBGA1023/Kconfig \
	src/cpu/intel/socket_FCBGA559/Kconfig \
	src/cpu/intel/socket_BGA1284/Kconfig \
	src/cpu/intel/socket_BGA956/Kconfig \
	src/cpu/intel/slot_1/Kconfig \
	src/cpu/intel/haswell/Kconfig \
	src/cpu/intel/model_f4x/Kconfig \
	src/cpu/intel/model_f3x/Kconfig \
	src/cpu/intel/model_f2x/Kconfig \
	src/cpu/intel/model_2065x/Kconfig \
	src/cpu/intel/fsp_model_406dx/Kconfig \
	src/cpu/intel/fsp_model_206ax/Kconfig \
	src/cpu/intel/model_206ax/Kconfig \
	src/cpu/intel/model_106cx/Kconfig \
	src/cpu/intel/model_1067x/Kconfig \
	src/cpu/intel/model_6fx/Kconfig \
	src/cpu/intel/model_6ex/Kconfig \
	src/cpu/intel/model_6dx/Kconfig \
	src/cpu/intel/model_6bx/Kconfig \
	src/cpu/intel/model_69x/Kconfig \
	src/cpu/intel/model_68x/Kconfig \
	src/cpu/intel/model_67x/Kconfig \
	src/cpu/intel/model_65x/Kconfig \
	src/cpu/intel/model_6xx/Kconfig \
	src/cpu/armltd/cortex-a9/Kconfig \
	src/cpu/amd/pi/00660F01/Kconfig \
	src/cpu/amd/pi/00730F01/Kconfig \
	src/cpu/amd/pi/00630F01/Kconfig \
	src/cpu/amd/pi/Kconfig \
	src/cpu/amd/agesa/family16kb/Kconfig \
	src/cpu/amd/agesa/family15tn/Kconfig \
	src/cpu/amd/agesa/family14/Kconfig \
	src/cpu/amd/agesa/family12/Kconfig \
	src/cpu/amd/agesa/Kconfig \
	src/cpu/amd/geode_lx/Kconfig \
	src/cpu/amd/family_10h-family_15h/Kconfig \
	src/cpu/amd/socket_F_1207/Kconfig \
	src/cpu/amd/socket_ASB2/Kconfig \
	src/cpu/amd/socket_G34/Kconfig \
	src/cpu/amd/socket_FM2/Kconfig \
	src/cpu/amd/socket_C32/Kconfig \
	src/cpu/amd/socket_AM3/Kconfig \
	src/cpu/amd/socket_AM2r2/Kconfig \
	src/cpu/allwinner/a10/Kconfig \
	src/cpu/allwinner/Kconfig \
	src/cpu/amd/Kconfig \
	src/cpu/armltd/Kconfig \
	src/cpu/intel/Kconfig \
	src/cpu/qemu-power8/Kconfig \
	src/cpu/qemu-x86/Kconfig \
	src/cpu/ti/Kconfig \
	src/cpu/via/Kconfig \
	src/cpu/x86/Kconfig \
	src/cpu/Kconfig \
	src/soc/ucb/riscv/Kconfig \
	src/soc/sifive/fu540/Kconfig \
	src/soc/samsung/exynos5250/Kconfig \
	src/soc/samsung/exynos5420/Kconfig \
	src/soc/rockchip/rk3288/Kconfig \
	src/soc/rockchip/rk3399/Kconfig \
	src/soc/qualcomm/ipq40xx/Kconfig \
	src/soc/qualcomm/ipq806x/Kconfig \
	src/soc/qualcomm/sdm845/Kconfig \
	src/soc/nvidia/tegra124/Kconfig \
	src/soc/nvidia/tegra210/Kconfig \
	src/soc/mediatek/mt8173/Kconfig \
	src/soc/mediatek/mt8183/Kconfig \
	src/soc/lowrisc/lowrisc/Kconfig \
	src/soc/intel/common/basecode/Kconfig \
	src/soc/intel/common/pch/lockdown/Kconfig \
	src/soc/intel/common/pch/Kconfig \
	src/soc/intel/common/block/acpi/Kconfig \
	src/soc/intel/common/block/chip/Kconfig \
	src/soc/intel/common/block/cpu/Kconfig \
	src/soc/intel/common/block/cse/Kconfig \
	src/soc/intel/common/block/dsp/Kconfig \
	src/soc/intel/common/block/ebda/Kconfig \
	src/soc/intel/common/block/fast_spi/Kconfig \
	src/soc/intel/common/block/gpio/Kconfig \
	src/soc/intel/common/block/graphics/Kconfig \
	src/soc/intel/common/block/gspi/Kconfig \
	src/soc/intel/common/block/hda/Kconfig \
	src/soc/intel/common/block/i2c/Kconfig \
	src/soc/intel/common/block/itss/Kconfig \
	src/soc/intel/common/block/lpc/Kconfig \
	src/soc/intel/common/block/lpss/Kconfig \
	src/soc/intel/common/block/p2sb/Kconfig \
	src/soc/intel/common/block/pcie/Kconfig \
	src/soc/intel/common/block/pcr/Kconfig \
	src/soc/intel/common/block/pmc/Kconfig \
	src/soc/intel/common/block/rtc/Kconfig \
	src/soc/intel/common/block/sata/Kconfig \
	src/soc/intel/common/block/scs/Kconfig \
	src/soc/intel/common/block/sgx/Kconfig \
	src/soc/intel/common/block/smbus/Kconfig \
	src/soc/intel/common/block/smm/Kconfig \
	src/soc/intel/common/block/spi/Kconfig \
	src/soc/intel/common/block/sram/Kconfig \
	src/soc/intel/common/block/systemagent/Kconfig \
	src/soc/intel/common/block/timer/Kconfig \
	src/soc/intel/common/block/uart/Kconfig \
	src/soc/intel/common/block/vmx/Kconfig \
	src/soc/intel/common/block/xdci/Kconfig \
	src/soc/intel/common/block/xhci/Kconfig \
	src/soc/intel/common/block/Kconfig \
	src/soc/intel/common/Kconfig \
	src/soc/intel/skylake/Kconfig \
	src/soc/intel/quark/Kconfig \
	src/soc/intel/fsp_broadwell_de/fsp/Kconfig \
	src/soc/intel/fsp_broadwell_de/Kconfig \
	src/soc/intel/fsp_baytrail/fsp/Kconfig \
	src/soc/intel/fsp_baytrail/Kconfig \
	src/soc/intel/denverton_ns/Kconfig \
	src/soc/intel/cannonlake/Kconfig \
	src/soc/intel/broadwell/Kconfig \
	src/soc/intel/braswell/Kconfig \
	src/soc/intel/baytrail/Kconfig \
	src/soc/intel/apollolake/Kconfig \
	src/soc/imgtec/pistachio/Kconfig \
	src/soc/cavium/cn81xx/Kconfig \
	src/soc/cavium/common/Kconfig \
	src/soc/broadcom/cygnus/Kconfig \
	src/soc/amd/common/block/cpu/Kconfig \
	src/soc/amd/common/block/pci/Kconfig \
	src/soc/amd/common/block/pi/Kconfig \
	src/soc/amd/common/block/psp/Kconfig \
	src/soc/amd/common/block/s3/Kconfig \
	src/soc/amd/common/block/Kconfig \
	src/soc/amd/common/Kconfig \
	src/soc/amd/stoneyridge/Kconfig \
	src/soc/amd/Kconfig \
	src/soc/broadcom/Kconfig \
	src/soc/cavium/Kconfig \
	src/soc/imgtec/Kconfig \
	src/soc/intel/Kconfig \
	src/soc/lowrisc/Kconfig \
	src/soc/mediatek/Kconfig \
	src/soc/nvidia/Kconfig \
	src/soc/qualcomm/Kconfig \
	src/soc/rockchip/Kconfig \
	src/soc/samsung/Kconfig \
	src/soc/sifive/Kconfig \
	src/soc/ucb/Kconfig \
	src/mainboard/via/epia-m850/Kconfig \
	src/mainboard/via/epia-m850/Kconfig.name \
	src/mainboard/tyan/s2912_fam10/Kconfig \
	src/mainboard/tyan/s2912_fam10/Kconfig.name \
	src/mainboard/ti/beaglebone/Kconfig \
	src/mainboard/ti/beaglebone/Kconfig.name \
	src/mainboard/supermicro/h8dmr_fam10/Kconfig \
	src/mainboard/supermicro/h8qme_fam10/Kconfig \
	src/mainboard/supermicro/h8scm_fam10/Kconfig \
	src/mainboard/supermicro/h8dmr_fam10/Kconfig.name \
	src/mainboard/supermicro/h8qme_fam10/Kconfig.name \
	src/mainboard/supermicro/h8scm_fam10/Kconfig.name \
	src/mainboard/sifive/hifive-unleashed/Kconfig \
	src/mainboard/sifive/hifive-unleashed/Kconfig.name \
	src/mainboard/siemens/mc_apl1/variants/mc_apl1/Kconfig \
	src/mainboard/siemens/mc_apl1/Kconfig \
	src/mainboard/siemens/mc_bdx1/Kconfig \
	src/mainboard/siemens/mc_tcu3/Kconfig \
	src/mainboard/siemens/mc_apl1/Kconfig.name \
	src/mainboard/siemens/mc_bdx1/Kconfig.name \
	src/mainboard/siemens/mc_tcu3/Kconfig.name \
	src/mainboard/scaleway/tagada/Kconfig \
	src/mainboard/scaleway/tagada/Kconfig.name \
	src/mainboard/sapphire/pureplatinumh61/Kconfig \
	src/mainboard/sapphire/pureplatinumh61/Kconfig.name \
	src/mainboard/samsung/lumpy/Kconfig \
	src/mainboard/samsung/stumpy/Kconfig \
	src/mainboard/samsung/lumpy/Kconfig.name \
	src/mainboard/samsung/stumpy/Kconfig.name \
	src/mainboard/roda/rk886ex/Kconfig \
	src/mainboard/roda/rk9/Kconfig \
	src/mainboard/roda/rv11/Kconfig \
	src/mainboard/roda/rk886ex/Kconfig.name \
	src/mainboard/roda/rk9/Kconfig.name \
	src/mainboard/roda/rv11/Kconfig.name \
	src/mainboard/purism/librem_bdw/Kconfig \
	src/mainboard/purism/librem_skl/Kconfig \
	src/mainboard/purism/librem_bdw/Kconfig.name \
	src/mainboard/purism/librem_skl/Kconfig.name \
	src/mainboard/pcengines/alix1c/Kconfig \
	src/mainboard/pcengines/alix2c/Kconfig \
	src/mainboard/pcengines/alix2d/Kconfig \
	src/mainboard/pcengines/alix6/Kconfig \
	src/mainboard/pcengines/apu1/Kconfig \
	src/mainboard/pcengines/apu2/Kconfig \
	src/mainboard/pcengines/alix1c/Kconfig.name \
	src/mainboard/pcengines/alix2c/Kconfig.name \
	src/mainboard/pcengines/alix2d/Kconfig.name \
	src/mainboard/pcengines/alix6/Kconfig.name \
	src/mainboard/pcengines/apu1/Kconfig.name \
	src/mainboard/pcengines/apu2/Kconfig.name \
	src/mainboard/packardbell/ms2290/Kconfig \
	src/mainboard/packardbell/ms2290/Kconfig.name \
	src/mainboard/opencellular/rotundu/Kconfig \
	src/mainboard/opencellular/rotundu/Kconfig.name \
	src/mainboard/ocp/monolake/Kconfig \
	src/mainboard/ocp/wedge100s/Kconfig \
	src/mainboard/ocp/monolake/Kconfig.name \
	src/mainboard/ocp/wedge100s/Kconfig.name \
	src/mainboard/msi/ms7721/Kconfig \
	src/mainboard/msi/ms9652_fam10/Kconfig \
	src/mainboard/msi/ms7721/Kconfig.name \
	src/mainboard/msi/ms9652_fam10/Kconfig.name \
	src/mainboard/lowrisc/nexys4ddr/Kconfig \
	src/mainboard/lowrisc/nexys4ddr/Kconfig.name \
	src/mainboard/lippert/frontrunner-af/Kconfig \
	src/mainboard/lippert/toucan-af/Kconfig \
	src/mainboard/lippert/frontrunner-af/Kconfig.name \
	src/mainboard/lippert/toucan-af/Kconfig.name \
	src/mainboard/lenovo/g505s/Kconfig \
	src/mainboard/lenovo/l520/Kconfig \
	src/mainboard/lenovo/r400/Kconfig \
	src/mainboard/lenovo/s230u/Kconfig \
	src/mainboard/lenovo/t400/Kconfig \
	src/mainboard/lenovo/t420/Kconfig \
	src/mainboard/lenovo/t420s/Kconfig \
	src/mainboard/lenovo/t430/Kconfig \
	src/mainboard/lenovo/t430s/Kconfig \
	src/mainboard/lenovo/t500/Kconfig \
	src/mainboard/lenovo/t520/Kconfig \
	src/mainboard/lenovo/t530/Kconfig \
	src/mainboard/lenovo/t60/Kconfig \
	src/mainboard/lenovo/x131e/Kconfig \
	src/mainboard/lenovo/x1_carbon_gen1/Kconfig \
	src/mainboard/lenovo/x200/Kconfig \
	src/mainboard/lenovo/x201/Kconfig \
	src/mainboard/lenovo/x220/Kconfig \
	src/mainboard/lenovo/x230/Kconfig \
	src/mainboard/lenovo/x60/Kconfig \
	src/mainboard/lenovo/z61t/Kconfig \
	src/mainboard/lenovo/g505s/Kconfig.name \
	src/mainboard/lenovo/l520/Kconfig.name \
	src/mainboard/lenovo/r400/Kconfig.name \
	src/mainboard/lenovo/s230u/Kconfig.name \
	src/mainboard/lenovo/t400/Kconfig.name \
	src/mainboard/lenovo/t420/Kconfig.name \
	src/mainboard/lenovo/t420s/Kconfig.name \
	src/mainboard/lenovo/t430/Kconfig.name \
	src/mainboard/lenovo/t430s/Kconfig.name \
	src/mainboard/lenovo/t500/Kconfig.name \
	src/mainboard/lenovo/t520/Kconfig.name \
	src/mainboard/lenovo/t530/Kconfig.name \
	src/mainboard/lenovo/t60/Kconfig.name \
	src/mainboard/lenovo/x131e/Kconfig.name \
	src/mainboard/lenovo/x1_carbon_gen1/Kconfig.name \
	src/mainboard/lenovo/x200/Kconfig.name \
	src/mainboard/lenovo/x201/Kconfig.name \
	src/mainboard/lenovo/x220/Kconfig.name \
	src/mainboard/lenovo/x230/Kconfig.name \
	src/mainboard/lenovo/x60/Kconfig.name \
	src/mainboard/lenovo/z61t/Kconfig.name \
	src/mainboard/kontron/986lcd-m/Kconfig \
	src/mainboard/kontron/ktqm77/Kconfig \
	src/mainboard/kontron/986lcd-m/Kconfig.name \
	src/mainboard/kontron/ktqm77/Kconfig.name \
	src/mainboard/jetway/nf81-t56n-lf/Kconfig \
	src/mainboard/jetway/pa78vm5/Kconfig \
	src/mainboard/jetway/nf81-t56n-lf/Kconfig.name \
	src/mainboard/jetway/pa78vm5/Kconfig.name \
	src/mainboard/intel/apollolake_rvp/Kconfig \
	src/mainboard/intel/baskingridge/Kconfig \
	src/mainboard/intel/bayleybay_fsp/Kconfig \
	src/mainboard/intel/camelbackmountain_fsp/Kconfig \
	src/mainboard/intel/cannonlake_rvp/Kconfig \
	src/mainboard/intel/coffeelake_rvp/Kconfig \
	src/mainboard/intel/cougar_canyon2/Kconfig \
	src/mainboard/intel/d510mo/Kconfig \
	src/mainboard/intel/d945gclf/Kconfig \
	src/mainboard/intel/dcp847ske/Kconfig \
	src/mainboard/intel/dg41wv/Kconfig \
	src/mainboard/intel/dg43gt/Kconfig \
	src/mainboard/intel/emeraldlake2/Kconfig \
	src/mainboard/intel/galileo/Kconfig \
	src/mainboard/intel/glkrvp/Kconfig \
	src/mainboard/intel/harcuvar/Kconfig \
	src/mainboard/intel/kblrvp/Kconfig \
	src/mainboard/intel/kunimitsu/Kconfig \
	src/mainboard/intel/leafhill/Kconfig \
	src/mainboard/intel/littleplains/Kconfig \
	src/mainboard/intel/minnow3/Kconfig \
	src/mainboard/intel/minnowmax/Kconfig \
	src/mainboard/intel/mohonpeak/Kconfig \
	src/mainboard/intel/saddlebrook/Kconfig \
	src/mainboard/intel/stargo2/Kconfig \
	src/mainboard/intel/strago/Kconfig \
	src/mainboard/intel/wtm2/Kconfig \
	src/mainboard/intel/apollolake_rvp/Kconfig.name \
	src/mainboard/intel/baskingridge/Kconfig.name \
	src/mainboard/intel/bayleybay_fsp/Kconfig.name \
	src/mainboard/intel/camelbackmountain_fsp/Kconfig.name \
	src/mainboard/intel/cannonlake_rvp/Kconfig.name \
	src/mainboard/intel/coffeelake_rvp/Kconfig.name \
	src/mainboard/intel/cougar_canyon2/Kconfig.name \
	src/mainboard/intel/d510mo/Kconfig.name \
	src/mainboard/intel/d945gclf/Kconfig.name \
	src/mainboard/intel/dcp847ske/Kconfig.name \
	src/mainboard/intel/dg41wv/Kconfig.name \
	src/mainboard/intel/dg43gt/Kconfig.name \
	src/mainboard/intel/emeraldlake2/Kconfig.name \
	src/mainboard/intel/galileo/Kconfig.name \
	src/mainboard/intel/glkrvp/Kconfig.name \
	src/mainboard/intel/harcuvar/Kconfig.name \
	src/mainboard/intel/kblrvp/Kconfig.name \
	src/mainboard/intel/kunimitsu/Kconfig.name \
	src/mainboard/intel/leafhill/Kconfig.name \
	src/mainboard/intel/littleplains/Kconfig.name \
	src/mainboard/intel/minnow3/Kconfig.name \
	src/mainboard/intel/minnowmax/Kconfig.name \
	src/mainboard/intel/mohonpeak/Kconfig.name \
	src/mainboard/intel/saddlebrook/Kconfig.name \
	src/mainboard/intel/stargo2/Kconfig.name \
	src/mainboard/intel/strago/Kconfig.name \
	src/mainboard/intel/wtm2/Kconfig.name \
	src/mainboard/iei/kino-780am2-fam10/Kconfig \
	src/mainboard/iei/kino-780am2-fam10/Kconfig.name \
	src/mainboard/ibase/mb899/Kconfig \
	src/mainboard/ibase/mb899/Kconfig.name \
	src/mainboard/hp/2570p/Kconfig \
	src/mainboard/hp/2760p/Kconfig \
	src/mainboard/hp/8460p/Kconfig \
	src/mainboard/hp/8470p/Kconfig \
	src/mainboard/hp/8770w/Kconfig \
	src/mainboard/hp/abm/Kconfig \
	src/mainboard/hp/compaq_8200_elite_sff/Kconfig \
	src/mainboard/hp/dl165_g6_fam10/Kconfig \
	src/mainboard/hp/folio_9470m/Kconfig \
	src/mainboard/hp/pavilion_m6_1035dx/Kconfig \
	src/mainboard/hp/revolve_810_g1/Kconfig \
	src/mainboard/hp/2570p/Kconfig.name \
	src/mainboard/hp/2760p/Kconfig.name \
	src/mainboard/hp/8460p/Kconfig.name \
	src/mainboard/hp/8470p/Kconfig.name \
	src/mainboard/hp/8770w/Kconfig.name \
	src/mainboard/hp/abm/Kconfig.name \
	src/mainboard/hp/compaq_8200_elite_sff/Kconfig.name \
	src/mainboard/hp/dl165_g6_fam10/Kconfig.name \
	src/mainboard/hp/folio_9470m/Kconfig.name \
	src/mainboard/hp/pavilion_m6_1035dx/Kconfig.name \
	src/mainboard/hp/revolve_810_g1/Kconfig.name \
	src/mainboard/google/auron/Kconfig \
	src/mainboard/google/beltino/Kconfig \
	src/mainboard/google/butterfly/Kconfig \
	src/mainboard/google/cheza/Kconfig \
	src/mainboard/google/cyan/Kconfig \
	src/mainboard/google/daisy/Kconfig \
	src/mainboard/google/eve/Kconfig \
	src/mainboard/google/fizz/Kconfig \
	src/mainboard/google/foster/Kconfig \
	src/mainboard/google/gale/Kconfig \
	src/mainboard/google/glados/Kconfig \
	src/mainboard/google/gru/Kconfig \
	src/mainboard/google/jecht/Kconfig \
	src/mainboard/google/kahlee/Kconfig \
	src/mainboard/google/kukui/Kconfig \
	src/mainboard/google/link/Kconfig \
	src/mainboard/google/nyan/Kconfig \
	src/mainboard/google/nyan_big/Kconfig \
	src/mainboard/google/nyan_blaze/Kconfig \
	src/mainboard/google/oak/Kconfig \
	src/mainboard/google/octopus/Kconfig \
	src/mainboard/google/parrot/Kconfig \
	src/mainboard/google/peach_pit/Kconfig \
	src/mainboard/google/poppy/Kconfig \
	src/mainboard/google/purin/Kconfig \
	src/mainboard/google/rambi/Kconfig \
	src/mainboard/google/reef/Kconfig \
	src/mainboard/google/slippy/Kconfig \
	src/mainboard/google/smaug/Kconfig \
	src/mainboard/google/storm/Kconfig \
	src/mainboard/google/stout/Kconfig \
	src/mainboard/google/urara/Kconfig \
	src/mainboard/google/veyron/Kconfig \
	src/mainboard/google/veyron_mickey/Kconfig \
	src/mainboard/google/veyron_rialto/Kconfig \
	src/mainboard/google/zoombini/Kconfig \
	src/mainboard/google/auron/Kconfig.name \
	src/mainboard/google/beltino/Kconfig.name \
	src/mainboard/google/butterfly/Kconfig.name \
	src/mainboard/google/cheza/Kconfig.name \
	src/mainboard/google/cyan/Kconfig.name \
	src/mainboard/google/daisy/Kconfig.name \
	src/mainboard/google/eve/Kconfig.name \
	src/mainboard/google/fizz/Kconfig.name \
	src/mainboard/google/foster/Kconfig.name \
	src/mainboard/google/gale/Kconfig.name \
	src/mainboard/google/glados/Kconfig.name \
	src/mainboard/google/gru/Kconfig.name \
	src/mainboard/google/jecht/Kconfig.name \
	src/mainboard/google/kahlee/Kconfig.name \
	src/mainboard/google/kukui/Kconfig.name \
	src/mainboard/google/link/Kconfig.name \
	src/mainboard/google/nyan/Kconfig.name \
	src/mainboard/google/nyan_big/Kconfig.name \
	src/mainboard/google/nyan_blaze/Kconfig.name \
	src/mainboard/google/oak/Kconfig.name \
	src/mainboard/google/octopus/Kconfig.name \
	src/mainboard/google/parrot/Kconfig.name \
	src/mainboard/google/peach_pit/Kconfig.name \
	src/mainboard/google/poppy/Kconfig.name \
	src/mainboard/google/purin/Kconfig.name \
	src/mainboard/google/rambi/Kconfig.name \
	src/mainboard/google/reef/Kconfig.name \
	src/mainboard/google/slippy/Kconfig.name \
	src/mainboard/google/smaug/Kconfig.name \
	src/mainboard/google/storm/Kconfig.name \
	src/mainboard/google/stout/Kconfig.name \
	src/mainboard/google/urara/Kconfig.name \
	src/mainboard/google/veyron/Kconfig.name \
	src/mainboard/google/veyron_mickey/Kconfig.name \
	src/mainboard/google/veyron_rialto/Kconfig.name \
	src/mainboard/google/zoombini/Kconfig.name \
	src/mainboard/gizmosphere/gizmo/Kconfig \
	src/mainboard/gizmosphere/gizmo2/Kconfig \
	src/mainboard/gizmosphere/gizmo/Kconfig.name \
	src/mainboard/gizmosphere/gizmo2/Kconfig.name \
	src/mainboard/gigabyte/ga-945gcm-s2l/Kconfig \
	src/mainboard/gigabyte/ga-b75m-d3h/Kconfig \
	src/mainboard/gigabyte/ga-b75m-d3v/Kconfig \
	src/mainboard/gigabyte/ga-g41m-es2l/Kconfig \
	src/mainboard/gigabyte/ga-h61m-s2pv/Kconfig \
	src/mainboard/gigabyte/ma785gm/Kconfig \
	src/mainboard/gigabyte/ma785gmt/Kconfig \
	src/mainboard/gigabyte/ma78gm/Kconfig \
	src/mainboard/gigabyte/ga-945gcm-s2l/Kconfig.name \
	src/mainboard/gigabyte/ga-b75m-d3h/Kconfig.name \
	src/mainboard/gigabyte/ga-b75m-d3v/Kconfig.name \
	src/mainboard/gigabyte/ga-g41m-es2l/Kconfig.name \
	src/mainboard/gigabyte/ga-h61m-s2pv/Kconfig.name \
	src/mainboard/gigabyte/ma785gm/Kconfig.name \
	src/mainboard/gigabyte/ma785gmt/Kconfig.name \
	src/mainboard/gigabyte/ma78gm/Kconfig.name \
	src/mainboard/getac/p470/Kconfig \
	src/mainboard/getac/p470/Kconfig.name \
	src/mainboard/foxconn/d41s/Kconfig \
	src/mainboard/foxconn/g41s-k/Kconfig \
	src/mainboard/foxconn/d41s/Kconfig.name \
	src/mainboard/foxconn/g41s-k/Kconfig.name \
	src/mainboard/facebook/watson/Kconfig \
	src/mainboard/facebook/watson/Kconfig.name \
	src/mainboard/esd/atom15/Kconfig \
	src/mainboard/esd/atom15/Kconfig.name \
	src/mainboard/emulation/qemu-armv7/Kconfig \
	src/mainboard/emulation/qemu-i440fx/Kconfig \
	src/mainboard/emulation/qemu-power8/Kconfig \
	src/mainboard/emulation/qemu-q35/Kconfig \
	src/mainboard/emulation/qemu-riscv/Kconfig \
	src/mainboard/emulation/spike-riscv/Kconfig \
	src/mainboard/emulation/qemu-armv7/Kconfig.name \
	src/mainboard/emulation/qemu-i440fx/Kconfig.name \
	src/mainboard/emulation/qemu-power8/Kconfig.name \
	src/mainboard/emulation/qemu-q35/Kconfig.name \
	src/mainboard/emulation/qemu-riscv/Kconfig.name \
	src/mainboard/emulation/spike-riscv/Kconfig.name \
	src/mainboard/elmex/pcm205400/Kconfig \
	src/mainboard/elmex/pcm205401/Kconfig \
	src/mainboard/elmex/pcm205400/Kconfig.name \
	src/mainboard/elmex/pcm205401/Kconfig.name \
	src/mainboard/cubietech/cubieboard/Kconfig \
	src/mainboard/cubietech/cubieboard/Kconfig.name \
	src/mainboard/compulab/intense_pc/Kconfig \
	src/mainboard/compulab/intense_pc/Kconfig.name \
	src/mainboard/cavium/cn8100_sff_evb/Kconfig \
	src/mainboard/cavium/cn8100_sff_evb/Kconfig.name \
	src/mainboard/biostar/a68n_5200/Kconfig \
	src/mainboard/biostar/am1ml/Kconfig \
	src/mainboard/biostar/a68n_5200/Kconfig.name \
	src/mainboard/biostar/am1ml/Kconfig.name \
	src/mainboard/bap/ode_e20XX/Kconfig \
	src/mainboard/bap/ode_e21XX/Kconfig \
	src/mainboard/bap/ode_e20XX/Kconfig.name \
	src/mainboard/bap/ode_e21XX/Kconfig.name \
	src/mainboard/avalue/eax-785e/Kconfig \
	src/mainboard/avalue/eax-785e/Kconfig.name \
	src/mainboard/asus/am1i-a/Kconfig \
	src/mainboard/asus/f2a85-m/Kconfig \
	src/mainboard/asus/kcma-d8/Kconfig \
	src/mainboard/asus/kfsn4-dre/Kconfig \
	src/mainboard/asus/kgpe-d16/Kconfig \
	src/mainboard/asus/m4a78-em/Kconfig \
	src/mainboard/asus/m4a785-m/Kconfig \
	src/mainboard/asus/m4a785t-m/Kconfig \
	src/mainboard/asus/m5a88-v/Kconfig \
	src/mainboard/asus/maximus_iv_gene-z/Kconfig \
	src/mainboard/asus/p2b-d/Kconfig \
	src/mainboard/asus/p2b-ds/Kconfig \
	src/mainboard/asus/p2b-f/Kconfig \
	src/mainboard/asus/p2b-ls/Kconfig \
	src/mainboard/asus/p2b/Kconfig \
	src/mainboard/asus/p3b-f/Kconfig \
	src/mainboard/asus/p5gc-mx/Kconfig \
	src/mainboard/asus/p8h61-m_lx/Kconfig \
	src/mainboard/asus/p8h61-m_pro/Kconfig \
	src/mainboard/asus/am1i-a/Kconfig.name \
	src/mainboard/asus/f2a85-m/Kconfig.name \
	src/mainboard/asus/kcma-d8/Kconfig.name \
	src/mainboard/asus/kfsn4-dre/Kconfig.name \
	src/mainboard/asus/kgpe-d16/Kconfig.name \
	src/mainboard/asus/m4a78-em/Kconfig.name \
	src/mainboard/asus/m4a785-m/Kconfig.name \
	src/mainboard/asus/m4a785t-m/Kconfig.name \
	src/mainboard/asus/m5a88-v/Kconfig.name \
	src/mainboard/asus/maximus_iv_gene-z/Kconfig.name \
	src/mainboard/asus/p2b-d/Kconfig.name \
	src/mainboard/asus/p2b-ds/Kconfig.name \
	src/mainboard/asus/p2b-f/Kconfig.name \
	src/mainboard/asus/p2b-ls/Kconfig.name \
	src/mainboard/asus/p2b/Kconfig.name \
	src/mainboard/asus/p3b-f/Kconfig.name \
	src/mainboard/asus/p5gc-mx/Kconfig.name \
	src/mainboard/asus/p8h61-m_lx/Kconfig.name \
	src/mainboard/asus/p8h61-m_pro/Kconfig.name \
	src/mainboard/asrock/b75pro3-m/Kconfig \
	src/mainboard/asrock/e350m1/Kconfig \
	src/mainboard/asrock/g41c-gs/Kconfig \
	src/mainboard/asrock/imb-a180/Kconfig \
	src/mainboard/asrock/b75pro3-m/Kconfig.name \
	src/mainboard/asrock/e350m1/Kconfig.name \
	src/mainboard/asrock/g41c-gs/Kconfig.name \
	src/mainboard/asrock/imb-a180/Kconfig.name \
	src/mainboard/apple/macbook21/Kconfig \
	src/mainboard/apple/macbookair4_2/Kconfig \
	src/mainboard/apple/macbook21/Kconfig.name \
	src/mainboard/apple/macbookair4_2/Kconfig.name \
	src/mainboard/aopen/dxplplusu/Kconfig \
	src/mainboard/aopen/dxplplusu/Kconfig.name \
	src/mainboard/amd/bettong/Kconfig \
	src/mainboard/amd/bimini_fam10/Kconfig \
	src/mainboard/amd/db-ft3b-lc/Kconfig \
	src/mainboard/amd/gardenia/Kconfig \
	src/mainboard/amd/inagua/Kconfig \
	src/mainboard/amd/lamar/Kconfig \
	src/mainboard/amd/mahogany_fam10/Kconfig \
	src/mainboard/amd/olivehill/Kconfig \
	src/mainboard/amd/olivehillplus/Kconfig \
	src/mainboard/amd/parmer/Kconfig \
	src/mainboard/amd/persimmon/Kconfig \
	src/mainboard/amd/serengeti_cheetah_fam10/Kconfig \
	src/mainboard/amd/south_station/Kconfig \
	src/mainboard/amd/thatcher/Kconfig \
	src/mainboard/amd/tilapia_fam10/Kconfig \
	src/mainboard/amd/torpedo/Kconfig \
	src/mainboard/amd/union_station/Kconfig \
	src/mainboard/amd/bettong/Kconfig.name \
	src/mainboard/amd/bimini_fam10/Kconfig.name \
	src/mainboard/amd/db-ft3b-lc/Kconfig.name \
	src/mainboard/amd/gardenia/Kconfig.name \
	src/mainboard/amd/inagua/Kconfig.name \
	src/mainboard/amd/lamar/Kconfig.name \
	src/mainboard/amd/mahogany_fam10/Kconfig.name \
	src/mainboard/amd/olivehill/Kconfig.name \
	src/mainboard/amd/olivehillplus/Kconfig.name \
	src/mainboard/amd/parmer/Kconfig.name \
	src/mainboard/amd/persimmon/Kconfig.name \
	src/mainboard/amd/serengeti_cheetah_fam10/Kconfig.name \
	src/mainboard/amd/south_station/Kconfig.name \
	src/mainboard/amd/thatcher/Kconfig.name \
	src/mainboard/amd/tilapia_fam10/Kconfig.name \
	src/mainboard/amd/torpedo/Kconfig.name \
	src/mainboard/amd/union_station/Kconfig.name \
	src/mainboard/advansus/a785e-i/Kconfig \
	src/mainboard/advansus/a785e-i/Kconfig.name \
	src/mainboard/adi/rcc-dff/Kconfig \
	src/mainboard/adi/rcc-dff/Kconfig.name \
	src/mainboard/adi/Kconfig \
	src/mainboard/adlink/Kconfig \
	src/mainboard/advansus/Kconfig \
	src/mainboard/amd/Kconfig \
	src/mainboard/aopen/Kconfig \
	src/mainboard/apple/Kconfig \
	src/mainboard/asrock/Kconfig \
	src/mainboard/asus/Kconfig \
	src/mainboard/avalue/Kconfig \
	src/mainboard/bap/Kconfig \
	src/mainboard/biostar/Kconfig \
	src/mainboard/cavium/Kconfig \
	src/mainboard/compulab/Kconfig \
	src/mainboard/cubietech/Kconfig \
	src/mainboard/elmex/Kconfig \
	src/mainboard/emulation/Kconfig \
	src/mainboard/esd/Kconfig \
	src/mainboard/facebook/Kconfig \
	src/mainboard/foxconn/Kconfig \
	src/mainboard/getac/Kconfig \
	src/mainboard/gigabyte/Kconfig \
	src/mainboard/gizmosphere/Kconfig \
	src/mainboard/google/Kconfig \
	src/mainboard/hp/Kconfig \
	src/mainboard/ibase/Kconfig \
	src/mainboard/iei/Kconfig \
	src/mainboard/intel/Kconfig \
	src/mainboard/jetway/Kconfig \
	src/mainboard/kontron/Kconfig \
	src/mainboard/lenovo/Kconfig \
	src/mainboard/lippert/Kconfig \
	src/mainboard/lowrisc/Kconfig \
	src/mainboard/msi/Kconfig \
	src/mainboard/ocp/Kconfig \
	src/mainboard/opencellular/Kconfig \
	src/mainboard/packardbell/Kconfig \
	src/mainboard/pcengines/Kconfig \
	src/mainboard/purism/Kconfig \
	src/mainboard/roda/Kconfig \
	src/mainboard/samsung/Kconfig \
	src/mainboard/sapphire/Kconfig \
	src/mainboard/scaleway/Kconfig \
	src/mainboard/siemens/Kconfig \
	src/mainboard/sifive/Kconfig \
	src/mainboard/supermicro/Kconfig \
	src/mainboard/ti/Kconfig \
	src/mainboard/tyan/Kconfig \
	src/mainboard/via/Kconfig \
	src/mainboard/adi/Kconfig.name \
	src/mainboard/adlink/Kconfig.name \
	src/mainboard/advansus/Kconfig.name \
	src/mainboard/amd/Kconfig.name \
	src/mainboard/aopen/Kconfig.name \
	src/mainboard/apple/Kconfig.name \
	src/mainboard/asrock/Kconfig.name \
	src/mainboard/asus/Kconfig.name \
	src/mainboard/avalue/Kconfig.name \
	src/mainboard/bap/Kconfig.name \
	src/mainboard/biostar/Kconfig.name \
	src/mainboard/cavium/Kconfig.name \
	src/mainboard/compulab/Kconfig.name \
	src/mainboard/cubietech/Kconfig.name \
	src/mainboard/elmex/Kconfig.name \
	src/mainboard/emulation/Kconfig.name \
	src/mainboard/esd/Kconfig.name \
	src/mainboard/facebook/Kconfig.name \
	src/mainboard/foxconn/Kconfig.name \
	src/mainboard/getac/Kconfig.name \
	src/mainboard/gigabyte/Kconfig.name \
	src/mainboard/gizmosphere/Kconfig.name \
	src/mainboard/google/Kconfig.name \
	src/mainboard/hp/Kconfig.name \
	src/mainboard/ibase/Kconfig.name \
	src/mainboard/iei/Kconfig.name \
	src/mainboard/intel/Kconfig.name \
	src/mainboard/jetway/Kconfig.name \
	src/mainboard/kontron/Kconfig.name \
	src/mainboard/lenovo/Kconfig.name \
	src/mainboard/lippert/Kconfig.name \
	src/mainboard/lowrisc/Kconfig.name \
	src/mainboard/msi/Kconfig.name \
	src/mainboard/ocp/Kconfig.name \
	src/mainboard/opencellular/Kconfig.name \
	src/mainboard/packardbell/Kconfig.name \
	src/mainboard/pcengines/Kconfig.name \
	src/mainboard/purism/Kconfig.name \
	src/mainboard/roda/Kconfig.name \
	src/mainboard/samsung/Kconfig.name \
	src/mainboard/sapphire/Kconfig.name \
	src/mainboard/scaleway/Kconfig.name \
	src/mainboard/siemens/Kconfig.name \
	src/mainboard/sifive/Kconfig.name \
	src/mainboard/supermicro/Kconfig.name \
	src/mainboard/ti/Kconfig.name \
	src/mainboard/tyan/Kconfig.name \
	src/mainboard/via/Kconfig.name \
	src/mainboard/Kconfig \
	src/Kconfig

build/auto.conf: \
	$(deps_config)


$(deps_config): ;
