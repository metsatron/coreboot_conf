#include <device/device.h>
#include <device/pci.h>
#include "cpu/intel/model_206ax/chip.h"
#include "drivers/pc80/tpm/chip.h"
#include "drivers/ricoh/rce822/chip.h"
#include "ec/lenovo/h8/chip.h"
#include "ec/lenovo/pmh7/chip.h"
#include "northbridge/intel/sandybridge/chip.h"
#include "southbridge/intel/bd82x6x/chip.h"

#if !DEVTREE_EARLY
__attribute__((weak)) struct chip_operations mainboard_ops = {};
__attribute__((weak)) struct chip_operations cpu_intel_model_206ax_ops = {};
__attribute__((weak)) struct chip_operations cpu_intel_socket_rPGA989_ops = {};
__attribute__((weak)) struct chip_operations drivers_i2c_at24rf08c_ops = {};
__attribute__((weak)) struct chip_operations drivers_pc80_tpm_ops = {};
__attribute__((weak)) struct chip_operations drivers_ricoh_rce822_ops = {};
__attribute__((weak)) struct chip_operations ec_lenovo_h8_ops = {};
__attribute__((weak)) struct chip_operations ec_lenovo_pmh7_ops = {};
__attribute__((weak)) struct chip_operations northbridge_intel_sandybridge_ops = {};
__attribute__((weak)) struct chip_operations southbridge_intel_bd82x6x_ops = {};
#endif
DEVTREE_CONST struct cpu_intel_model_206ax_config cpu_intel_model_206ax_info_5 = {
	.c1_acpower = 1,
	.c1_battery = 1,
	.c2_acpower = 3,
	.c2_battery = 3,
	.c3_acpower = 5,
	.c3_battery = 5,
};

DEVTREE_CONST struct drivers_pc80_tpm_config drivers_pc80_tpm_info_35 = {};

DEVTREE_CONST struct drivers_ricoh_rce822_config drivers_ricoh_rce822_info_21 = {
	.disable_mask = 0x87,
	.sdwppol = 1,
};

DEVTREE_CONST struct ec_lenovo_h8_config ec_lenovo_h8_info_37 = {
	.beepmask0 = 0x00,
	.beepmask1 = 0x86,
	.config0 = 0xa6,
	.config1 = 0x09,
	.config2 = 0xa0,
	.config3 = 0xe0,
	.event2_enable = 0xff,
	.event3_enable = 0xff,
	.event4_enable = 0xd0,
	.event5_enable = 0xfc,
	.event6_enable = 0x00,
	.event7_enable = 0x01,
	.event8_enable = 0x7b,
	.event9_enable = 0xff,
	.eventa_enable = 0x01,
	.eventb_enable = 0x00,
	.eventc_enable = 0xff,
	.eventd_enable = 0xff,
	.evente_enable = 0x0d,
	.has_bdc_detection = 0,
	.has_keyboard_backlight = 1,
	.has_power_management_beeps = 0,
	.has_wwan_detection = 1,
	.wwan_gpio_lvl = 0,
	.wwan_gpio_num = 70,
};

DEVTREE_CONST struct ec_lenovo_pmh7_config ec_lenovo_pmh7_info_33 = {
	.backlight_enable = 0x01,
	.dock_event_enable = 0x01,
};

DEVTREE_CONST struct northbridge_intel_sandybridge_config northbridge_intel_sandybridge_info_1 = {
	.gfx.did = { 0x80000100, 0x80000240, 0x80000410, 0x80000410, 0x00000005 },
	.gfx.link_frequency_270_mhz = 1,
	.gfx.ndid = 3,
	.gfx.use_spread_spectrum_clock = 1,
	.gpu_cpu_backlight = 0x1155,
	.gpu_dp_d_hotplug = 0x06,
	.gpu_panel_port_select = 0,
	.gpu_panel_power_backlight_off_delay = 2100,
	.gpu_panel_power_backlight_on_delay = 2100,
	.gpu_panel_power_cycle_delay = 6,
	.gpu_panel_power_down_delay = 100,
	.gpu_panel_power_up_delay = 100,
	.gpu_pch_backlight = 0x11551155,
	.pci_mmio_size = 1024,
};

DEVTREE_CONST struct southbridge_intel_bd82x6x_config southbridge_intel_bd82x6x_info_11 = {
	.alt_gp_smi_en = 0x0000,
	.c2_latency = 101,
	.gen1_dec = 0x7c1601,
	.gen2_dec = 0x0c15e1,
	.gen4_dec = 0x0c06a1,
	.gpi13_routing = 2,
	.gpi1_routing = 2,
	.p_cnt_throttling_supported = 1,
	.pcie_hotplug_map = { 0, 0, 1, 0, 0, 0, 0, 0 },
	.pcie_port_coalesce = 1,
	.sata_interface_speed_support = 0x3,
	.sata_port_map = 0x7,
	.spi_lvscc = 0x2005,
	.spi_uvscc = 0x2005,
	.superspeed_capable_ports = 0xf,
	.xhci_overcurrent_mapping = 0x4000201,
	.xhci_switchable_ports = 0xf,
};


/* pass 0 */
DEVTREE_CONST struct bus dev_root_links[];
DEVTREE_CONST static struct device _dev2;
DEVTREE_CONST struct bus _dev2_links[];
DEVTREE_CONST static struct device _dev7;
DEVTREE_CONST struct bus _dev7_links[];
DEVTREE_CONST static struct device _dev4;
DEVTREE_CONST static struct device _dev6;
DEVTREE_CONST static struct device _dev8;
DEVTREE_CONST static struct device _dev9;
DEVTREE_CONST static struct device _dev10;
DEVTREE_CONST static struct device _dev12;
DEVTREE_CONST static struct device _dev13;
DEVTREE_CONST static struct device _dev14;
DEVTREE_CONST static struct device _dev15;
DEVTREE_CONST static struct device _dev16;
DEVTREE_CONST static struct device _dev17;
DEVTREE_CONST static struct device _dev18;
DEVTREE_CONST static struct device _dev19;
DEVTREE_CONST static struct device _dev20;
DEVTREE_CONST struct bus _dev20_links[];
DEVTREE_CONST static struct device _dev23;
DEVTREE_CONST static struct device _dev24;
DEVTREE_CONST static struct device _dev25;
DEVTREE_CONST static struct device _dev26;
DEVTREE_CONST static struct device _dev27;
DEVTREE_CONST static struct device _dev28;
DEVTREE_CONST static struct device _dev29;
DEVTREE_CONST static struct device _dev30;
DEVTREE_CONST static struct device _dev31;
DEVTREE_CONST static struct device _dev32;
DEVTREE_CONST struct bus _dev32_links[];
DEVTREE_CONST static struct device _dev39;
DEVTREE_CONST static struct device _dev40;
DEVTREE_CONST struct bus _dev40_links[];
DEVTREE_CONST static struct device _dev50;
DEVTREE_CONST static struct device _dev51;
DEVTREE_CONST static struct device _dev22;
DEVTREE_CONST static struct device _dev34;
DEVTREE_CONST static struct device _dev36;
DEVTREE_CONST static struct device _dev38;
DEVTREE_CONST struct resource _dev38_res[];
DEVTREE_CONST static struct device _dev42;
DEVTREE_CONST static struct device _dev43;
DEVTREE_CONST static struct device _dev44;
DEVTREE_CONST static struct device _dev45;
DEVTREE_CONST static struct device _dev46;
DEVTREE_CONST static struct device _dev47;
DEVTREE_CONST static struct device _dev48;
DEVTREE_CONST static struct device _dev49;
DEVTREE_CONST struct device * DEVTREE_CONST last_dev = &_dev49;

/* pass 1 */
DEVTREE_CONST struct device dev_root = {
#if !DEVTREE_EARLY
	.ops = &default_dev_ops_root,
#endif
	.bus = &dev_root_links[0],
	.path = { .type = DEVICE_PATH_ROOT },
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = &dev_root_links[0],
#if !DEVTREE_EARLY
	.chip_ops = &mainboard_ops,
	.name = mainboard_name,
#endif
	.next=&_dev2
};
DEVTREE_CONST struct bus dev_root_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &dev_root,
			.children = &_dev2,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev2 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &dev_root_links[0],
	.path = {.type=DEVICE_PATH_CPU_CLUSTER,{.cpu_cluster={ .cluster = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = &_dev2_links[0],
	.sibling = &_dev7,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev7
};
DEVTREE_CONST struct bus _dev2_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev2,
			.children = &_dev4,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev7 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &dev_root_links[0],
	.path = {.type=DEVICE_PATH_DOMAIN,{.domain={ .domain = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = &_dev7_links[0],
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev4
};
DEVTREE_CONST struct bus _dev7_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev7,
			.children = &_dev8,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev4 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev2_links[0],
	.path = {.type=DEVICE_PATH_APIC,{.apic={ .apic_id = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev6,
#if !DEVTREE_EARLY
	.chip_ops = &cpu_intel_socket_rPGA989_ops,
#endif
	.next=&_dev6
};
static DEVTREE_CONST struct device _dev6 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev2_links[0],
	.path = {.type=DEVICE_PATH_APIC,{.apic={ .apic_id = 0xacac }}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &cpu_intel_model_206ax_ops,
#endif
	.chip_info = &cpu_intel_model_206ax_info_5,
	.next=&_dev8
};
static DEVTREE_CONST struct device _dev8 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x0,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev9,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev9
};
static DEVTREE_CONST struct device _dev9 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1,0)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev10,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev10
};
static DEVTREE_CONST struct device _dev10 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x2,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev12,
#if !DEVTREE_EARLY
	.chip_ops = &northbridge_intel_sandybridge_ops,
#endif
	.chip_info = &northbridge_intel_sandybridge_info_1,
	.next=&_dev12
};
static DEVTREE_CONST struct device _dev12 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x14,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev13,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev13
};
static DEVTREE_CONST struct device _dev13 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev14,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev14
};
static DEVTREE_CONST struct device _dev14 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,1)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev15,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev15
};
static DEVTREE_CONST struct device _dev15 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,2)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev16,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev16
};
static DEVTREE_CONST struct device _dev16 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x16,3)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev17,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev17
};
static DEVTREE_CONST struct device _dev17 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x19,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21f3,
	.link_list = NULL,
	.sibling = &_dev18,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev18
};
static DEVTREE_CONST struct device _dev18 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1a,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev19,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev19
};
static DEVTREE_CONST struct device _dev19 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1b,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev20,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev20
};
static DEVTREE_CONST struct device _dev20 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = &_dev20_links[0],
	.sibling = &_dev23,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev23
};
DEVTREE_CONST struct bus _dev20_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev20,
			.children = &_dev22,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev23 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,1)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev24,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev24
};
static DEVTREE_CONST struct device _dev24 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,2)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev25,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev25
};
static DEVTREE_CONST struct device _dev25 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,3)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev26,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev26
};
static DEVTREE_CONST struct device _dev26 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,4)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev27,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev27
};
static DEVTREE_CONST struct device _dev27 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,5)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev28,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev28
};
static DEVTREE_CONST struct device _dev28 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,6)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev29,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev29
};
static DEVTREE_CONST struct device _dev29 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1c,7)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev30,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev30
};
static DEVTREE_CONST struct device _dev30 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1d,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev31,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev31
};
static DEVTREE_CONST struct device _dev31 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1e,0)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev32,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev32
};
static DEVTREE_CONST struct device _dev32 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = &_dev32_links[0],
	.sibling = &_dev39,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev39
};
DEVTREE_CONST struct bus _dev32_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev32,
			.children = &_dev34,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev39 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,2)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
	.sibling = &_dev40,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev40
};
static DEVTREE_CONST struct device _dev40 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,3)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = &_dev40_links[0],
	.sibling = &_dev50,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev50
};
DEVTREE_CONST struct bus _dev40_links[] = {
		[0] = {
			.link_num = 0,
			.dev = &_dev40,
			.children = &_dev42,
			.next = NULL,
		},
	};
static DEVTREE_CONST struct device _dev50 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,5)}}},
	.enabled = 0,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev51,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev51
};
static DEVTREE_CONST struct device _dev51 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev7_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x1f,6)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &southbridge_intel_bd82x6x_ops,
#endif
	.chip_info = &southbridge_intel_bd82x6x_info_11,
	.next=&_dev22
};
static DEVTREE_CONST struct device _dev22 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev20_links[0],
	.path = {.type=DEVICE_PATH_PCI,{.pci={ .devfn = PCI_DEVFN(0x0,0)}}},
	.enabled = 1,
	.on_mainboard = 1,
	.subsystem_vendor = 0x17aa,
	.subsystem_device = 0x21fa,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_ricoh_rce822_ops,
#endif
	.chip_info = &drivers_ricoh_rce822_info_21,
	.next=&_dev34
};
static DEVTREE_CONST struct device _dev34 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev32_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0xff, .device = 0x1 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev36,
#if !DEVTREE_EARLY
	.chip_ops = &ec_lenovo_pmh7_ops,
#endif
	.chip_info = &ec_lenovo_pmh7_info_33,
	.next=&_dev36
};
static DEVTREE_CONST struct device _dev36 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev32_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0xc31, .device = 0x0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev38,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_pc80_tpm_ops,
#endif
	.chip_info = &drivers_pc80_tpm_info_35,
	.next=&_dev38
};
static DEVTREE_CONST struct device _dev38 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev32_links[0],
	.path = {.type=DEVICE_PATH_PNP,{.pnp={ .port = 0xff, .device = 0x2 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.resource_list = &_dev38_res[0],
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &ec_lenovo_h8_ops,
#endif
	.chip_info = &ec_lenovo_h8_info_37,
	.next=&_dev42
};
DEVTREE_CONST struct resource _dev38_res[] = {
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x60, .base=0x62,.next=&_dev38_res[1]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x62, .base=0x66,.next=&_dev38_res[2]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x64, .base=0x1600,.next=&_dev38_res[3]},
		{ .flags=IORESOURCE_FIXED | IORESOURCE_ASSIGNED | IORESOURCE_IO, .index=0x66, .base=0x1604,.next=NULL },
	 };
static DEVTREE_CONST struct device _dev42 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x54, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev43,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev43
};
static DEVTREE_CONST struct device _dev43 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x55, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev44,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev44
};
static DEVTREE_CONST struct device _dev44 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x56, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev45,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev45
};
static DEVTREE_CONST struct device _dev45 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x57, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev46,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev46
};
static DEVTREE_CONST struct device _dev46 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x5c, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev47,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev47
};
static DEVTREE_CONST struct device _dev47 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x5d, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev48,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev48
};
static DEVTREE_CONST struct device _dev48 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x5e, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
	.sibling = &_dev49,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
	.next=&_dev49
};
static DEVTREE_CONST struct device _dev49 = {
#if !DEVTREE_EARLY
	.ops = NULL,
#endif
	.bus = &_dev40_links[0],
	.path = {.type=DEVICE_PATH_I2C,{.i2c={ .device = 0x5f, .mode_10bit = 0 }}},
	.enabled = 1,
	.on_mainboard = 1,
	.link_list = NULL,
#if !DEVTREE_EARLY
	.chip_ops = &drivers_i2c_at24rf08c_ops,
#endif
};
